package main

import "fmt"

type Payer interface {
	Pay(int) error
	GetCash() int
}

type Wallet struct {
	Cash int
}

func (w *Wallet) Pay(amount int) error {
	if w.Cash < amount {
		return fmt.Errorf("Не хватает денег в кошельке")
	}
	w.Cash -= amount
	return nil
}

func (w Wallet) GetCash() int {
	return w.Cash
}

func Buy(p Payer) Payer { // ей просто важно то, что придет в аргумент обладало методом Pay(int) error
	err := p.Pay(10) // а какая структура не важно
	if err != nil {
		panic(err)
	}
	fmt.Printf("Спасибо за покупку через %T\n\n", p)
	return p
}

// experiment

func NewWallet(amount int) Payer {
	w := &Wallet{Cash: 100}
	return w
}

func main() {
	newWallet := NewWallet(100)
	fmt.Println(newWallet.GetCash())
}
