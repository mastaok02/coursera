package main

import "fmt"

func main() {
	// инициализация при создании
	var user map[string]string = map[string]string{
		"name":     "Nartay",
		"lastName": "Dembayev",
	}

	// сразу с нужной емкостью, чтобы не приходилось расширять в runtime
	profile := make(map[string]string, 10)
	fmt.Println("profile", profile)

	// количество элементов
	mapLength := len(user)
	fmt.Println("mapLength", mapLength)

	// если ключа нет - вернет значение по умолчанию для типа
	mName := user["middleName"]
	fmt.Println("mName", mName)

	// проверка на существование ключа
	mName, mNameExist := user["middleName"]
	fmt.Println("mName", mName, "mNameExist", mNameExist)

	// удалекние ключа
	delete(user, "lastName")
	fmt.Println("user", user)
}
