package main

import "fmt"

func main() {
	// размер массив является частью его типа

	// инициализация значениями по-умолчанию
	var a1 [3]int // 000
	fmt.Println("a1 short", a1)
	fmt.Printf("a1 short %v\n", a1)
	fmt.Printf("a1 full %#v\n", a1)

	// для размера можно использовать переменную, но только const
	const size = 2
	var a2 [2 * size]bool // [false,false,false,false]
	fmt.Println("a2", a2)

	// определение размера при объявлении
	// [...] возьми столько элементов столько я скажу
	a3 := [...]int{1, 2, 3}
	fmt.Println("a3", a3)

	// проверка при компиляции или при выполнении
	// invalid array index 4 (out of bounds for 3-element array)
	// a3[4] = 12

	// experiment
	// если в index ставить переменную, то это будет проверено в runtime и завершится паникой
	a := 4
	a3[a] = 4
}
