package main

import "fmt"

func main() {
	// У slice есть его длина и capacity
	// длина - количество элементов которая УЖЕ есть
	// capacity - количество элементов которая влезеть в этот slice без аллоцирование памяти

	// при вызове метода make() идет аллокация нового массива
	// которая возвращает новый слайс ссылающееся на тот новый массив

	// создание
	var buf0 []int             // len=0, cap=0
	buf1 := []int{}            // len=0, cap=0
	buf2 := []int{42}          // len=1, cap=1
	buf3 := make([]int, 0)     // len=0, cap=0
	buf4 := make([]int, 5)     // len=5, cap=5
	buf5 := make([]int, 5, 10) // len=5, cap=10
	fmt.Println("buf5", buf5, "cap", cap(buf5))

	fmt.Println(buf0, buf1, buf2, buf3, buf4)

	// обращение к элементам
	someInt := buf2[0]

	// ошибка при выполнении
	// panic: runtime error: index out of range
	// someOtherInt := buf2[1]

	fmt.Println(someInt)

	// добавление элементов
	// при увелечение размерности runtime делает x2 capacity от предведущего размера
	var buf []int            // len=0, cap=0
	buf = append(buf, 9, 10) // len=2, cap=2
	buf = append(buf, 12)    // len=3, cap=4

	// добавление другого слайса
	otherBuf := make([]int, 3)     // [0, 0, 0]
	buf = append(buf, otherBuf...) // len=6, cap=8

	// experiment
	// а что будет если добавить больше чем x2 от предведущего размера
	otherBuf2 := make([]int, 10)           // [0, 0, 0]
	buf = append(buf, otherBuf2...)        // len=6, cap=8
	fmt.Println("buf", len(buf), cap(buf)) // len=13, cap=14
	// не понятно почему там cap=14, если он должен был увеличиться на x2 2 раза, то есть 8 и 16

	// https://go.dev/blog/slices-intro
	// Нарезка не копирует данные среза. Он создает новое значение среза, которое указывает на исходный массив.
	// Это делает операции среза столь же эффективными, как и манипулирование индексами массива.
	// Следовательно, изменение элементов (а не самого среза) повторного среза изменяет элементы исходного фрагмента:
	d := []byte{'r', 'o', 'a', 'd'}
	e := d[2:]
	// e == []byte{'a', 'd'}
	e[1] = 'm'
	// e == []byte{'a', 'm'}
	// d == []byte{'r', 'o', 'a', 'm'}

	// test append Byte
	a := make([]byte, 2)
	fmt.Println("Before a", len(a), cap(a))
	a = AppendByte(a, 'r')
	fmt.Println("After a", len(a), cap(a))

	// experiment
	c := []string{"1", "2"}
	cSlice := c[:]
	cSlice[0] = "3"
	fmt.Println("c", c)
	fmt.Println("cSlice", cSlice)

	cSlice = append(cSlice, "1")
	fmt.Println("append c", c)
	fmt.Println("append cSlice", cSlice)

	cSlice[0] = "9"
	fmt.Println("change c", c)
	fmt.Println("change cSlice", cSlice)
}

// https://go.dev/blog/slices-intro
// append ручками
// допустуим slice len=2, cap=2
// и добавлем 1 элемент
func AppendByte(slice []byte, data ...byte) []byte {
	m := len(slice)     // длина текущего slice = 2
	n := m + len(data)  // длина данных которых нужно добавить 1 + 2 = 3
	if n > cap(slice) { // if necessary, reallocate
		// allocate double what's needed, for future growth.
		newSlice := make([]byte, (n+1)*2) // (3+1) * 2 = 8
		copy(newSlice, slice)             // копируем туда данные
		slice = newSlice
	}
	slice = slice[0:n] // 0...3
	copy(slice[m:n], data)
	// len=3, cap=8????
	return slice
}
