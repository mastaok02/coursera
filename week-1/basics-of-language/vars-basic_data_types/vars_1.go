package main

import "fmt"

func main() {
	// значение по умолчанию
	// для int значение по умолчанию = 0
	var num0 int
	fmt.Println("num0", num0) // 0

	// значение при инициальизации
	var num1 int = 1
	fmt.Println("num1", num1) // 1

	// пропуск типа
	// компилятор автоматический определяет тип
	var num2 = 20
	fmt.Println("num2", num2) // 20

	// короткое объявление переменной
	num := 30
	// только для новых переменных

	// есть var +=1
	num += 1
	fmt.Println("num +=", num)

	// но нет ++var, зато есть var++, то есть 2 плюса после var
	num++
	fmt.Println("num ++", num)

	// camelCase - принятый стиль
	userIndex := 10
	// under_score - не принято
	user_index := 10
	fmt.Println(userIndex, user_index)

	// объявление несколких переменных
	var weight, height int = 10, 20

	// присваивание в существующие переменные
	weight, height = 11, 21

	// коротокое присваивание
	// хотя-бы одна переменная должна быть новой
	// если одна новая то другой можно присвоить значение
	weight, age := 12, 22
	fmt.Println("weight", weight)
	fmt.Println("height", height)
	fmt.Println("age", age)
}
