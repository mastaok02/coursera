package main

import "fmt"

type UserID int

func main() {
	idx := 1
	var uid UserID = 42

	// даже если базовый тип одинаковый, разные типы несовместимы
	// cannot use uid (type UserId) as type int64 in assignment
	// myID := idx

	myID := UserID(idx)

	println(uid, myID)

	// experiment
	a := new(UserID)
	fmt.Println("a", *a)
}
