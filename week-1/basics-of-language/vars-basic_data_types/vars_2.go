package main

import "fmt"

func main() {
	// int - платформазависимый типа, 32/64
	var i int = 10
	fmt.Println("i", i) // 10

	// автоматический выбранный int
	var autoInt = -10

	// int8, int16, int32, int64
	var bigInt int64 = 1<<32 - 1
	fmt.Println("bigInt", bigInt)

	// платформозависимый тип, 32/64. Всегда плюс, то есть всегда позитивное число
	var unsignedInt uint = 1000500
	fmt.Println("unsignedInt", unsignedInt)

	// uint8, uint16, uint32, uint64
	// uint8 или int8 максимум 255 или -255
	var unsignedBigInt uint8 = 255
	fmt.Println("unsignedBigInt", unsignedBigInt)
	fmt.Println(autoInt)

	// float32, float64
	var pi float32 = 103333333333330000000000009999999999999.01
	var e = 2.718
	goldenRatio := 1.168
	var defaultFloatValue float64
	fmt.Println("defaultFloatValue", defaultFloatValue)

	fmt.Println("Float numbers", pi, e, goldenRatio)

	// complex65, complex 128
	var c complex128 = -1.1 + 7.12i
	c2 := -1.1 + 7.12i

	fmt.Println("Complex numbers", c, c2)
}
