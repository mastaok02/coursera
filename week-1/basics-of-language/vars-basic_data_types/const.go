package main

import "fmt"

const pi = 3.141

// блок констант
const (
	hello = "Привет"
	e     = 2.718
)

// iota - автоинкремент для констант
const (
	zero = iota
	_    // пустая переменная, пропуск iota
	_
	three // = 3
)

const (
	_         = iota             // пропускаем первое значение
	KB uint64 = 1 << (10 * iota) // 1024
	MB                           //1048576
)

// EXPERIMENT
const (
	_ = iota
	a = 2 * (1 + iota) // 4
	b                  // 6
)

// нетипизированная константа остается нетипизированной,
// оно типизируется когда подставляется в нужное место
// иногда из за этого const могут иметь целочисленное значение превышаеющее 64 бит
const (
	// нетипизированная константа
	year = 2017
	// типизированная константа
	yearTyped int = 2017
)

func main() {
	fmt.Println(zero, three)
	fmt.Println(1 << 10) // KB
	fmt.Println(1 << 20) // MB
	fmt.Println(b)

	var month int32 = 13
	fmt.Println(month + year)

	// month + yearTyped(mismatched types int32 and int)
	// fmt.Println(month + yearTyped)
}
