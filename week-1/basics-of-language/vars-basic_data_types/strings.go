package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {
	// пустая строка по-умолчанию
	var str string
	fmt.Println("str", str) // "" -  пустая строка

	// со спец символами перенос строки или табуляция и тд
	var hello string = "Привет \n\t"
	fmt.Println("hello", hello)

	// UTF-8 из коробки поддерживается
	var helloWorld = "Привет, Мир!"
	fmt.Println("helloWorld", helloWorld)

	// одинарные кавычки для байт (uint8)
	var rawBinary byte = '\x27'
	fmt.Println("rawBinary", rawBinary)

	// rune (uint32) для UTF-8 символов
	var someRussian rune = 'р'
	fmt.Println("someRussian", someRussian)
	fmt.Println(uint32(someRussian))

	helloWorld = "Привет Мир"
	// конкатенация строк
	andGoodMorning := helloWorld + " и доброе утро!"
	fmt.Println(andGoodMorning)

	// строки неизменяемы
	// Ошибка - cannot assign to helloWorld[0]
	// helloWorld[0] = 72

	// получение длины строки
	// если попытаться получить длину строки через len(), то получим длину в байтах
	byteLen := len(helloWorld)                    // 19 байт
	symbols := utf8.RuneCountInString(helloWorld) // 10 рн

	fmt.Println("byteLen", byteLen)
	fmt.Println("symbols", symbols)

	// получение подстроки, в байтах, не символах!
	// по байтам получаем
	hello2 := helloWorld[:12] // Привет, 0-11 байты
	H := helloWorld[0]        // byte, 208, не "П"
	fmt.Println("hello2", hello2)
	fmt.Println("H", H)
	fmt.Println("Experiment byte to str", string(H)) // Ð

	// конвертация в слайс байт и обратно
	byteString := []byte(helloWorld)
	helloWorld = string(byteString)
	fmt.Println(byteString, helloWorld)
}
