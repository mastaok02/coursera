package main

import "fmt"

func main() {
	// цикл без условия, while(true) OR for(;;;) {
	for {
		fmt.Println("loop iteration")
		break
	}

	// цикл без условия, while(isRun)
	isRun := true
	for isRun {
		fmt.Println("loop iteration with condition")
		isRun = false
	}

	// цикл с условием и блоком инициализации
	for i := 0; i < 2; i++ {
		fmt.Println("loop iteration", i)
		if i == 1 {
			continue
		}
	}

	// операция по slice
	s1 := []int{1, 2, 3}
	idx := 0
	for idx < len(s1) {
		fmt.Println("while -stype loop, idx:", idx, "value:", s1[idx])
		idx++
	}

	for idx := range s1 {
		fmt.Println("range slice by index", idx)
	}

	str := "Привет, Мир!"
	for pos, char := range str {
		fmt.Printf("%#U at pos %d\n", char, pos)
	}
}
