package main

import "fmt"

func main() {
	// простое условие
	boolValue := true
	if boolValue {
		fmt.Println("true")
	}

	mapVal := map[string]string{"name": "rsvsiliy"}
	if keyValue, keyExist := mapVal["name"]; keyExist {
		fmt.Println("name =", keyValue)
	}

	cond := 1
	// ножественные if else
	if cond == 1 {
		fmt.Println("cond is 1")
	} else if cond == 2 {
		fmt.Println("cond is 2")
	}

	// switch по 1 переменной
	strVal := "name"
	switch strVal {
	case "name":
		fallthrough
	case "test", "lastName":
	//some work
	default:
		//some work
	}

	// switch как замена многим ifelse
	var val1, val2 = 2, 2
	switch {
	case val1 > 1 || val2 < 11:
		fmt.Println("first block")
	case val2 > 10:
		fmt.Println("second block")
	}

}
