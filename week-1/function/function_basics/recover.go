package main

import "fmt"

func deferTest() {
	defer func() {
		err := recover()
		if err != nil {
			fmt.Println("There was a panic with this err", err)
		}
	}()
	fmt.Println("Some useful work")
	panic("Something bad happened")
	return
}

func main() {
	deferTest()
	// experiment
	fmt.Println("AAAA")
	return
}
