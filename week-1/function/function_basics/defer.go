package main

import "fmt"

// АРГУМЕНТЫ отложенных функции вычесляется
// при объявлении отложенной функции (блок defer)
// То есть когда объявляется defer выполняется функция
// которая находится в аргументе

func getSomeVars() string {
	fmt.Println("getSomeVars execution")
	return "getSomeVars result"
}
func main() {
	// defer fmt.Println("After work")
	// defer fmt.Println(getSomeVars())
	// fmt.Println("Some useful work")

	// "getSomeVars execution"
	// "Some useful work"
	// "getSomeVars result"
	// "After work"

	defer fmt.Println("After work")
	defer func() {
		fmt.Println(getSomeVars())
	}()
	fmt.Println("Some useful work")

	// "Some useful work"
	// "getSomeVars execution"
	// "getSomeVars result"
	// "After work"
}
