package main

import "fmt"

type MySlice []int

func (mySlice *MySlice) Add(number int) {
	*mySlice = append(*mySlice, number)
}
func main() {
	sl := MySlice([]int{1, 2})
	sl.Add(3)
	fmt.Printf("%#v \n", sl)
}
